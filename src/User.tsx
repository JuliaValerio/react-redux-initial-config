import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { setUserName } from './store/modules/user/actions'

import api from './services/api'

import { ApplicationState } from './store'

function ConsumingApi() {
  const hello = useSelector((state: ApplicationState) => state.user.hello)
  const name = useSelector((state: ApplicationState) => state.user.name)

  const dispatch = useDispatch()

  return (
    <div>
      <h1>Consuming API</h1>
      <h2>{hello}</h2>
      <h2>{name}</h2>
      <button onClick={() => dispatch(setUserName("Novo Nome"))}>Set Name</button>
    </div>
  )
}

export default ConsumingApi