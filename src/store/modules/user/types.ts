export enum UserTypes {
  'SET_USER_NAME' = '@user/SET_USER_NAME'
}

export interface UserState {
  hello: string
  name: string
}