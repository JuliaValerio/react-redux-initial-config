import { action } from 'typesafe-actions'
import { UserTypes } from './types'

export const setUserName = (name: string) => action(UserTypes.SET_USER_NAME, { name })
